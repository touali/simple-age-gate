<?php

namespace Drupal\simple_age_gate\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Theme\ActiveTheme;

/**
 * Provide a 'simple-age-gate' block.
 *
 * @Block(
 *   id = "simple_age_gate_block",
 *   admin_label = @Translation("Simple Age Gate Block"),
 *   category = @Translation("Simple age gate modal")
 * )
 */
class SimpleAgeGateBlock extends BlockBase implements BlockPluginInterface
{
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultConfig = \Drupal::config('simple_age_gate.settings');
    /** @var ActiveTheme $theme */
    $theme = \Drupal::service('theme.manager')->getActiveTheme();

    return [
      'age_gate_logo_path' => $theme->getPath() . '/logo.svg',
      'age_gate_text' => $defaultConfig->get('simple_age_gate.text'),
      'age_gate_button_label' => $defaultConfig->get('simple_age_gate.button_label'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $formState) {
    $form = parent::blockForm($form, $formState);

    $config = $this->getConfiguration();

    $form['age_gate_logo_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logo path'),
      '#description' => $this->t("Clear this field if you don't want to display the logo."),
       '#default_value' => isset($config['age_gate_logo_path']) ? $config['age_gate_logo_path'] : '',
    ];

    $form['age_gate_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Provide a text to display on the age gate content'),
      '#default_value' => isset($config['age_gate_text']) ? $config['age_gate_text'] : '',
    ];

    $form['age_gate_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label'),
      '#description' => $this->t('Provide a label for the validation button'),
      '#default_value' => isset($config['age_gate_button_label']) ? $config['age_gate_button_label'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $formState) {
    parent::blockSubmit($form, $formState);

    $this->configuration['age_gate_logo_path'] = $formState->getValue('age_gate_logo_path');
    $this->configuration['age_gate_text'] = $formState->getValue('age_gate_text');
    $this->configuration['age_gate_button_label'] = $formState->getValue('age_gate_button_label');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $logoPath = empty($config['age_gate_logo_path']) ? '' : $config['age_gate_logo_path'];
    $text = empty($config['age_gate_text']) ? '' : $config['age_gate_text'];
    $buttonLabel = empty($config['age_gate_button_label']) ? '' : $config['age_gate_button_label'];

    return array(
      '#theme' => 'simple_age_gate',
      '#logo_path' => $logoPath,
      '#text' => $text,
      '#button_label' => $buttonLabel,
      '#attached' => [
        'library' => [
          'simple_age_gate/global',
        ]
      ],
    );
  }
}