var SimpleAgeGate = function ($) {
    var object = function(modalID) {
        var that = this;

        this.modalID = modalID;
        this.modal = $(modalID);
        this.acceptButton = this.modal.find('.sag-btn-access');
        this.hasAccepted = false;

        this.init();

        if(!this.hasAccepted) {
            this.modal.show();

            if (this.acceptButton.length > 0) {
                this.acceptButton.click(function (e) {
                    that.accept();
                });
            }
        }
    };

    object.prototype = {
        init: function () {
            this.hasAccepted = Cookies.get('sag-has-accepted');

            if (this.hasAccepted) {
                this.hide();
            } else {
                this.show();
            }
        },
        show: function () {
            this.modal.removeClass('sag-hidden').addClass('sag-visible');
        },
        hide: function () {
            this.modal.removeClass('sag-visible').addClass('sag-hidden');
        },
        accept: function () {
            Cookies.set('sag-has-accepted', true, { expires: 15, path: '' });
            this.hide();
        }
    };

    return object;
}(jQuery);

(function($) {
    $(function () {
        new SimpleAgeGate('#block-simpleagegateblock');
    });
}(jQuery));
